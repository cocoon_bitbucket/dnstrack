package dnswatch


import (

	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"log"
)

var (
	name = "hive"

	url = "redis://192.168.99.100:6379/1"

	broker_available = false
)


func main() {


	hive := dnsbroker.NewHive(name,url)


	err := hive.Check(false)
	if err != nil {
		broker_available = false
		log.Printf("no broker available: standalone mode\n")
	} else {
		broker_available = true
		log.Printf("broker [%s] available at %s\n" ,hive.Name, hive.Url)
	}

	if broker_available == true {
		client := dnsbroker.NewRedisClient("me")
		client.Lock()
		client.Conn.Do("SET","dnswatch","starting")
		client.Subscribe("dnsguard/packets")
		client.Unlock()
		log.Printf("waiting for message on channel dnsguard/packets")
		for {
			message := client.Receive()
			log.Printf("received: \n%s\n ", message)

		}

	} else {

		log.Fatal("no broker available")
	}


}
