package dnsfilter

/*

	define the format of the dns message to send the store: event

	event is a go map



 */

import (
	"strings"
	"encoding/json"
	"fmt"
	"github.com/google/gopacket/layers"
	"golang.org/x/net/publicsuffix"
)

var(
	publisher_name = "dnsguard"
)





func MakeDnsMessages(pktId uint32, event map[string]interface{} ,dns  layers.DNS) []byte {

	event["dns.qr"] = dns.QR

	event["dns.id"] = dns.ID
	event["dns.op_code"] = dns.OpCode
	event["dns.response_code"] = dns.ResponseCode
	event["questions_count"] = dns.QDCount
	event["answers_count"] = dns.ANCount

	//event["dns.Questions"]= dns.Questions
	//event["dns.Answers"]= dns.Answers

	for _, dnsQuestion := range dns.Questions {
		//name:= fmt.Sprintf("")
		name := strings.ToLower(string(dnsQuestion.Name))
		event["dns.question.name"] = name
		eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
		if err == nil {
			event["dns.question.etld_plus_one"] = eTLDPlusOne + "."
		}
		event["dns.question.type"] = dnsQuestion.Type
		event["dns.question.class"] = dnsQuestion.Class
		// take only the first question
		break
	}

	//spew.Dump(event)

	message, _ := json.Marshal(event)
	fmt.Println(string(message))


	return message

}


