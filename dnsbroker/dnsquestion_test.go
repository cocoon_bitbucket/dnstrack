package dnsbroker_test




import(

	"testing"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"github.com/google/gopacket/layers"
	"github.com/davecgh/go-spew/spew"
	"fmt"
	"github.com/stretchr/testify/assert"
)




func TestGuardQuestion(t *testing.T) {


	domain := []byte("domain.io.")
	name := []byte("name.domain.io")

	b := dnsbroker.NewDnsGuardQuestion(
		1,
		domain,
		name,
		layers.DNSTypeA,
		layers.DNSClassIN,
	)

	spew.Dump(b)

	s := b.Marshal()
	fmt.Println(string(s))

	//var dat map[string]interface{}

	b2 := b.UnMarshal(s)
	spew.Dump(b2)

	//assert.Equal(t,b.QuestionName,b2.QuestionName)

}


func TestGuardQuestion2(t *testing.T) {


	//domain := []byte("domain.io.")

	domain := "domain.io."


	name := []byte("name.domain.io")

	question := layers.DNSQuestion{name,layers.DNSTypeA, layers.DNSClassIN}


	b := dnsbroker.NewDnsGuardQuestion2(
		1,
		domain,
		question,
	)

	spew.Dump(b)

	s := b.Marshal()
	fmt.Println(string(s))

	//var dat map[string]interface{}

	b2 := b.UnMarshal(s)
	spew.Dump(b2)

	assert.Equal(t,b.QuestionName,b2.QuestionName)

}



