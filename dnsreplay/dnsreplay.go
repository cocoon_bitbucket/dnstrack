package dnsreplay

// Use tcpdump to create a test file
// tcpdump -w test.pcap


import (
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsfilter"
	"log"
)


// a redis pubsub publisher
type RePlayer struct {

	pcapfile string
	//publisher * dnsfilter.Publisher
	cnf * dnsbroker.DnstrackConfig
	filter  dnsfilter.Filter
}


func NewReplayer(pcap_filename string, publisher dnsbroker.Broker) RePlayer {


	log.Printf("create new RePlayer for pcap file: %s\n",pcap_filename)

	// get the config Singleton
	cnf := dnsbroker.GetConfig()

	filter := dnsfilter.NewFilter(publisher)

	player := RePlayer{pcap_filename,cnf,filter}

	return player
}



func ( player * RePlayer) Start() () {
	//
	// open the pcapfile , read it , send packets to filter
	//

	// get the config singleton
	redis_url := player.cnf.Redis.Url

	// create the hive singleton for publisher
	dnsbroker.NewHive("hive",redis_url)

	player.Loop()


}

func ( player * RePlayer) Loop() () {

	// Open file instead of device

	handle, err := pcap.OpenOffline( player.pcapfile)
	if err != nil { log.Fatal(err) }
	defer handle.Close()
	log.Printf("replay pcap file %s \n",player.pcapfile)
	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		fmt.Println(packet)
		verdict,_ :=player.filter.Do(packet)
		if verdict == false {
			log.Printf("packet has been dropped")
		}

	}
	log.Printf("end of pcap file %s \n",player.pcapfile)

}


