package dnsreplay
/*


	WORK IN PROGRESS

	a variant of dnsreplay.go but using workerpool : https://github.com/gammazero/workerpool



 */


// Use tcpdump to create a test file
// tcpdump -w test.pcap


import (
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsfilter"
	"github.com/gammazero/workerpool"
	"log"
)


// a redis pubsub publisher
type WRePlayer struct {

	pcapfile string
	//publisher * dnsfilter.Publisher
	cnf * dnsbroker.DnstrackConfig
	filter  dnsfilter.Filter
	wpool * workerpool.WorkerPool
}


func NewWReplayer(pcap_filename string, publisher dnsbroker.Broker) WRePlayer {


	log.Printf("create new RePlayer for pcap file: %s\n",pcap_filename)

	// get the config Singleton
	cnf := dnsbroker.GetConfig()

	filter := dnsfilter.NewFilter(publisher)

	// create and start the worker pool
	pool := workerpool.New(100)

	player := WRePlayer{pcap_filename,cnf,filter,pool}

	return player
}


func ( player * WRePlayer) Stop() () {
	player.wpool.Stop()

}



func ( player * WRePlayer) Start() () {
	//
	// open the pcapfile , read it , send packets to filter
	//

	// get the config singleton
	redis_url := player.cnf.Redis.Url

	// create the hive singleton for publisher
	dnsbroker.NewHive("hive",redis_url)

	player.Loop()


}


func ( player * WRePlayer) Loop() () {

	// Open file instead of device

	handle, err := pcap.OpenOffline( player.pcapfile)
	if err != nil { log.Fatal(err) }
	defer handle.Close()
	log.Printf("replay pcap file %s \n",player.pcapfile)
	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {

		player.wpool.Submit(func() {
			fmt.Println(packet)
			verdict,_ :=player.filter.Do(packet)
			if verdict == false {
				log.Printf("packet has been dropped")
			}

		})



	}
	log.Printf("end of pcap file %s \n",player.pcapfile)

}


