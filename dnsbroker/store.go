/*

	a store based on redis


	set domains to blacklist
	get domains that are black listed


 */


package dnsbroker

import (

	"log"
	"fmt"
)

const (

	blacklist_prefix = "blacklist"

)

var (
	default_blacklist_ttl = 3600 * 24
)


type Store struct {

	RedisClient

}



func NewStore() * Store {


	client := NewRedisClient("store")

	store := Store{*client}

	return &store

}


func (store * Store) SetDomain ( domain string,ttl uint) error {

	// add a domain to the blacklist
	key := store.blackListKey(domain)
	store.Lock()
	_,err := store.Conn.Do("SET" ,key,".")
	store.Unlock()
	if err != nil {
			log.Printf("fail to set domain:%s ", err)
	} else {
		// set OK: try expire
		expire := fmt.Sprintf("%d", ttl)
		store.Lock()
		_, err = store.Conn.Do("EXPIRE",key, expire)
		store.Unlock()
		if err != nil {
			log.Printf("fail to set domain:%s ", err)
		}
	}
	return err
}

func (store * Store) CheckDomain ( domain string) bool {
	// check domain return False if domain is in blaklist
	// True if not in blacklist or cannot read db

	// add a domain to the blacklist
	key := store.blackListKey(domain)
	store.Lock()
	v,err := store.Conn.Do("EXISTS" ,key,".")
	store.Unlock()

	if err != nil {
		log.Printf("fail to exists domain:%s ",err)
		// cannot reach db: by default assume domain is ok
		return true
	}
	r := v.(int64)
	if r == 1 {
		// domain is in black list
		return false
	} else {
		// domain not in black list
		return true
	}
}


//func (store * Store) GetDomain ( domain string) (string,error) {
//
//	// add a domain to the blacklist
//
//	key := store.blackListKey(domain)
//	v,err := store.Conn.Do("GET",key)
//
//	if err != nil {
//		log.Printf("fail to get domain:%s ",err)
//		return "",err
//	}
//	value := ""
//	if v != nil {
//		value = string(v.([]byte))
//	}
//	return value,err
//}


func (store * Store) DelDomain ( domain string) error{

	// add a domain to the blacklist
	key := store.blackListKey(domain)
	store.Lock()
	_,err := store.Conn.Do("UNSET",key)
	store.Unlock()
	if err != nil {
		log.Printf("fail to remove domain:%s ",err)
	}
	return err
}

func (store * Store) blackListKey ( domain string)  string{

	//compute key
	key := blacklist_prefix + ":" + domain
	return key

}


