package dnsbroker

/*

	WORK IN PROGRESS ( not yet ready )

	a broker is link between a module ( like dnsguardian)  and the storage system (redis )

	it describes an interface with methods

	ExportPacket( packet ) to store the packet in the database

	PublishEvent( name, event ) , publish an event eg dropped packet

	IsDomainInBlacklist( domain) to ask database if the domain is in blacklist


 */

import (
	"github.com/garyburd/redigo/redis"
	//"sync"
	//"bitbucket.org/orangeparis/udnstrack/dnsbroker"
	//"fmt"
)



type Broker interface {

	// write the packet in the store  packet is either a go.packet or a dictionary
	// return True if packet has been stored and false if not
	ExportPacket (  packet interface{} ) bool

	// ask database if domain is in blacklist ( return true if domain is in blacklist false otherwise )
	// note: if database is not reachable : always return False
	IsDomainInBlacklist(  domain string) bool

	// create an event : eg a packet has been dropped
	PublishEvent ( name string ,event interface{}) bool

}


func  ExportPacket( broker Broker,packet interface{} ) bool {
	return broker.ExportPacket(packet)
}

func IsDomainInBlacklist(broker Broker,domain string) bool {
	return broker.IsDomainInBlacklist(domain)
}

func PublishEvent ( broker Broker ,name string ,event interface{}) bool {
	return broker.PublishEvent(name,event)
}



/*

	a Simple broker implementation


	export packet on local redis database ( hit: )
	check blaklist localy ( blacklist: )

	publish event directly in redis ( drop: )


 */



//
//  simple broker , implements Broker interface
//
const (

	// prefix for keys to store data in redis database
	//blacklist_prefix = "blacklist"
	//whitelist_prefix = "whitelist"
	//counter_prefix = "hit"
	//drop_prefix = "drop"

	//parameters_prefix = "parameters"

	//drop_ttl = 3600
)



type SimpleBroker struct {

	// a simple broker store data directly in redis database
	RedisClient

}

func NewSimpleBroker( ) *SimpleBroker {


	client := NewRedisClient("broker")

	broker := SimpleBroker{*client}
	return &broker

}



func ( broker *SimpleBroker) ExportPacket( interface{}) bool {
	//

	// increment domain counter

	return true
}

func ( broker *SimpleBroker) PublishEvent( name string, event interface{}) bool {
	//

	// publish event

	return true
}

func ( broker *SimpleBroker) IsDomainInBlacklist ( domain string )  bool {
	// // check domain is in blacklist/whitelist
	exists ,err := broker.isInlist(blacklist_prefix,domain)
	if err != nil {
		// error reaching database: by default assume domain is not on blacklist
		exists = false
	}
	return exists
}


//
// private functions
//

func ( broker * SimpleBroker) isInlist ( prefix string ,name string) (bool,error) {
	// check domain is in blacklist/whitelist

	key := prefix + ":" + name

	// add a domain to the blacklist
	broker.Lock()
	//exists, err := broker.Do("EXISTS", key)

	exists, err := redis.Bool(broker.Conn.Do("EXISTS", key))
	broker.Unlock()
	return exists,err
}








