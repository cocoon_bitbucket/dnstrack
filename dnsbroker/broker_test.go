
package dnsbroker_test



import (
"testing"
"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
)


func TestSimpleBroker(t *testing.T) {


	name := "hive"
	url := "redis://localhost:6379/0"

	domain := "google.com."

	packet := make(map[string]string)
	packet["Id"] = "1"
	packet["Type"] = "dns"

	// create the hive singleton

	dnsbroker.ResetHive()
	dnsbroker.NewHive(name,url)

	broker := dnsbroker.NewSimpleBroker()

	b := broker.IsDomainInBlacklist(domain)
	println(b)

	r := broker.ExportPacket(packet)
	println(r)


}
//

func TestPubSubBroker(t *testing.T) {


	name := "hive"
	url := "redis://localhost:6379/0"

	domain := "google.com."

	packet := make(map[string]string)
	packet["Id"] = "1"
	packet["Type"] = "dns"

	// create the hive singleton

	dnsbroker.ResetHive()
	dnsbroker.NewHive(name,url)

	broker := dnsbroker.NewPubSubBroker()

	b := broker.IsDomainInBlacklist(domain)
	println(b)

	r := broker.ExportPacket(packet)
	println(r)


}