package main

import (
"log"
"bitbucket.org/cocoon_bitbucket/dnstrack/dnsguard"
"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"

)

var (
	name = "hive"
	url = "redis://redis:6379/0"

)

// iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
// iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0



func main() {


	// setup the context

	// create the configuration
	conf := dnsbroker.NewConfig("./dnstrak.toml")
	// create the hive
	dnsbroker.NewHive(name,conf.Redis.Url)


	// get the context
	//cnf := dnsbroker.GetConfig()


	log.Printf("starting dnsguardian")
	guard := dnsguard.NewGardian()
	defer guard.Close()

	guard.Start()


}

