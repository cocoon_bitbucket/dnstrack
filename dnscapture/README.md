WORK IN PROGRESS


# overview

register a pcap file of dns udp flow


for the moment capture the traffic with the command below


    sudo tcpdump -w test.pcap
    
    
# usage

capture packets with the cocoon/basedns image

build the image ( if necessary )

    cd docker/gobase
    docker rmi cocoon/gobase
    ./build
    
    
launch dns server in background
    
     docker run --privileged --network="host" cocoon/dns &
    
launch image

    docker run -it --privileged --network="host" cocoon/basedns ash
    
    
launch tcpdump 

    sudo tcpdump -w /tmp/trace.pcap
    

inject network traffic and stop the tcpdump with ctrl+C

    
get the /tmp/trace.pcap file





    
    
    