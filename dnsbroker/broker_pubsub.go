package dnsbroker


/*
	WORK IN PROGRESS

	a implementation of broker interface who publish on redis pubsub


	not yet ready


 */

import (
	"github.com/garyburd/redigo/redis"
	//"sync"
	//"bitbucket.org/orangeparis/udnstrack/dnsbroker"
)



//
// PubSubBroker , implements Broker interface
//

type PubSubBroker struct {

	// a broker which export packet via pubsub channel and ask blacklist directly

	RedisClient

}

func NewPubSubBroker( ) *PubSubBroker {


	client := NewRedisClient("broker")

	broker := PubSubBroker{*client}

	return &broker


}


func ( broker *PubSubBroker) ExportPacket( interface{}) bool {
	//
	return true
}

func ( broker *PubSubBroker) PublishEvent( name string, event interface{}) bool {
	//
	return true
}


func ( broker *PubSubBroker) IsDomainInBlacklist ( domain string )  bool {
	// // check domain is in blacklist/whitelist
	exists ,err := broker.isInlist(blacklist_prefix,domain)
	if err != nil {
		// error reaching database: by default assume domain is not on blacklist
		exists = false
	}
	return exists
}


//
// private functions
//

func ( broker * PubSubBroker) isInlist ( prefix string ,name string) (bool,error) {
	// check domain is in blacklist/whitelist

	key := prefix + ":" + name

	// add a domain to the blacklist
	broker.Lock()
	//exists, err := broker.Do("EXISTS", key)

	exists, err := redis.Bool(broker.Conn.Do("EXISTS", key))
	broker.Unlock()
	return exists,err
}

