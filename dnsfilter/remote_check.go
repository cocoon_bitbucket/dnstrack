package dnsfilter


import (
	"github.com/google/gopacket/layers"
	"golang.org/x/net/publicsuffix"
	"strings"
	//"log"
	//"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
)

var BlackList = map[string]bool {

	"orangedns.club.": true,
	//"orangedns.info.": true,
}



//func local_check(domain string) string {
//
//	verdict := "PASS"
//
//	_, ok := BlackList[domain]
//	if ok {
//		// domain is blacklistedv DROP it
//		log.Printf("domain is blacklisted: ", domain)
//		verdict = "DROP"
//
//	}
//	return verdict
//}


//func redis_check(domain string, publisher *Publisher) string {
//
//	//
//	//   check if domain is in blacklist
//	//			return "PASS" if not in blacklist or cannot reach redis
//
//	verdict := "PASS"
//
//	publisher.Lock()
//	r,err := publisher.Do("GET",domain)
//	publisher.Unlock()
//	if err != nil {
//		log.Printf("ERROR GET domain",err)
//
//	} else {
//		if r != nil {
//			// domain has been found in blacklist
//			verdict = "DROP"
//		}
//	}
//
//	return verdict
//
//}




func (filter * Filter) Check_blacklist_rules(dns layers.DNS) string {


	verdict:= "PASS"
	// check dns event with local rules
	for _, dnsQuestion := range dns.Questions {

		name := string(dnsQuestion.Name)
		eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
		if err == nil {
			eTLDPlusOne = eTLDPlusOne + "."
		} else {
			eTLDPlusOne = "unknown" + "."
		}

		//verdict= local_check(eTLDPlusOne)
		exists := filter.isDomainInBlacklist(eTLDPlusOne)
		if exists == true {
			verdict = "DROP"
			break
		}


		//_, ok := BlackList[eTLDPlusOne]
		//if ok {
		//	// domain is blacklistedv DROP it
		//	println("domain is blacklisted: ", eTLDPlusOne)
		//	verdict = "DROP"
		//	break
		//}
	}
	return verdict
}

