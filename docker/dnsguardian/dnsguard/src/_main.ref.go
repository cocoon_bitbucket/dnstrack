package main

import (
	"github.com/kung-foo/freki/netfilter"
	dnsguard "bitbucket.org/cocoon_bitbucket/dnstrack/dnsguard"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"

	"os"
	"log"
	"os/signal"
	"time"
)

var (
	name = "hive"
	url = "redis://redis:6379/1"
	broker_available = false
)





func onErrorExit(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func onInterruptSignal(fn func()) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	go func() {
		<-sig
		fn()
	}()
}

// iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
// iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0







func main() {



	hive := dnsbroker.NewHive(name,url)






	q, err := netfilter.New(0, 100, netfilter.NF_DEFAULT_PACKET_SIZE)
	onErrorExit(err)

	pp := 0

	onInterruptSignal(func() {
		q.Close()
		log.Printf("\n%d packets processed", pp)
		os.Exit(0)
	})

	// check redis connectivity
	log.Printf("wait for redis connection\n")
	time.Sleep(2)

	err = hive.Check(false)
	if err != nil {
		broker_available = false
		log.Printf("no broker available: standalone mode\n")
	} else {
		broker_available = true
		log.Printf("broker [%s] available at %s\n" ,hive.Name, hive.Url)
	}

	if broker_available == true {
		client := dnsbroker.NewRedisClient("me")
		client.Lock()
		client.Conn.Do("SET","hello","world")
		client.Unlock()
	}


	dnsguard.Handle_dns(q)

	//go q.Run()
	//
	//pChan := q.Packets()
	//
	//for p := range pChan {
	//	log.Printf("packet received\n")
	//	q.SetVerdict(p, netfilter.NF_ACCEPT)
	//	pp++
	//}
}

