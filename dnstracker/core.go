//package dnstracker

/*

	WORK IN PROGRESS

	replace dnsguard  to use dnsfilter module




 */

package dnstracker

import (
	"github.com/kung-foo/freki/netfilter"
	"os"
	"log"
	"os/signal"
	//"github.com/davecgh/go-spew/spew"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsfilter"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
)


func onErrorExit(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func onInterruptSignal(fn func()) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	go func() {
		<-sig
		fn()
	}()
}

// iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
// iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0

// a redis pubsub publisher
type Guardian struct {

	nfqueue * netfilter.Queue

	//cnf * dnsbroker.DnstrackConfig
	filter  dnsfilter.Filter
	counter uint
	//publisher * Publisher
}
//pcapfile string
//publisher * dnsfilter.Publisher



func NewGardian() *Guardian {


	log.Printf("create new guardian\n")

	nfqueue, err := netfilter.New(0, 1000, netfilter.NF_DEFAULT_PACKET_SIZE)
	onErrorExit(err)

	//onInterruptSignal(func() {
	//
	//	nfqueue.Close()
	//	log.Printf("\n%d packets processed", pp)
	//	os.Exit(0)
	//})

	//hive := dnsbroker.NewHive(name,url)

	//publisher := NewPublisher()
	publisher := dnsbroker.NewPubSubBroker()
	filter := dnsfilter.NewFilter(publisher)


	guardian := Guardian{nfqueue,filter,0}


	onInterruptSignal(func() {
		guardian.Close()
		//nfqueue.Close()
		//log.Printf("\n%d packets processed", pp)
		//os.Exit(0)
	})


	return &guardian
}


func (guard * Guardian) Close() {

	guard.nfqueue.Close()
	//guard.publisher.Close()
	log.Printf("Guardian closed: %d packets processed\n", guard.counter)
	os.Exit(-1)

}

func (guard * Guardian) Start() {

	// start nfqueue
	log.Printf("starting nfqueue\n")
	go guard.nfqueue.Run()
	log.Printf("nfqueue started\n")


	//packets := guard.nfqueue.Packets()

	guard.HandleDns()

}

func (guard * Guardian) HandleDns() {


	log.Printf("Handle Dns")
	// main loop
	pChan := guard.nfqueue.Packets()

	log.Printf("entering main HandleDns loop\n")

	for p := range pChan {
		//log.Printf("packet received here\n")

		//spew.Dump(p)
		// p is a netfilter rawPacket
		// p.ID is  netfilter packet id to set verdict on
		// p.Data is the raw content (ip layer) of the received packet )

		log.Printf("==================== PKT [%03d] \n", p.ID)
		// fmt.Printf("------------------------------------------------------------------------\n id: %d\n", payload.Id)
		// fmt.Println(hex.Dump(payload.Data))
		// Decode a packet

		verdict,err := guard.filter.Do(p)
		//println("dissector result:",err,"\n")
		if err != nil {
			println("filter error result:", err, "\n")
		} else {
			// success we have a verdict
		}

		if verdict == false {
			log.Printf("==================== DROP PACKET: [%03d]", p.ID)
			guard.nfqueue.SetVerdict(p, netfilter.NF_DROP)
		} else {
			log.Printf("===================== ACCEPT PACKET: [%03d]", p.ID)
			guard.nfqueue.SetVerdict(p, netfilter.NF_ACCEPT)
		}
		guard.counter++
		// tempo to slow down
		//time.Sleep(1 * time.Millisecond)
	}

	log.Printf("no more packets from nfqueue \n")

}


