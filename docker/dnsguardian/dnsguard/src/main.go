package main

import (

	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsguard"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"

)

var (
	name = "hive"
	url = "redis://redis:6379/0"

)

// iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
// iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0



func main() {



	dnsbroker.NewHive(name,url)

	guard := dnsguard.NewGardian()
	guard.Start()




}

