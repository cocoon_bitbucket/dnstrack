/*

	redis emiter



	hive:= NewHive("world" ," "redis://localhost:6379/1"")
	hive.Check(true)

	client := dnsbroker.NewRedisClient("me")

	client.Subscribe("me")
	client.Subscribe("me.*")

	client.Publish("me",text)
	client.Publish("me.info",text)

	client.Unsubscribe()





 */


package dnsbroker


import (
	"github.com/garyburd/redigo/redis"
	"sync"
	"time"
	//"log"
	"strings"
	"log"
)

// A pub-sub message - defined to support Redis receiving different
// message types, such as subscribe/unsubscribe info.
type RMessage struct {
	Type    string
	Channel string
	Data    string
	Pattern string
}

// Client interface for Messenger
type Client interface {

	Subscribe(channels ...interface{}) (err error)
	Unsubscribe(channels ...interface{}) (err error)
	Publish(channel string, message interface{} ) (int64,error)
	Receive() (message RMessage)


	//PSubscribe(channels ...interface{}) (err error)
	//PUnsubscribe(channels ...interface{}) (err error)

}

// Redis client - defines the underlying connection and pub-sub
// connections, as well as a mutex for locking write access,
// since this occurs from multiple goroutines.
type RedisClient struct {

	Name string
	hive * Hive
	redis.Conn
	PubSubConn redis.PubSubConn
	sync.Mutex
}


func NewRedisClient( name string) *RedisClient {

	hive := GetHive()
	conn := hive.NewConn()
	pubsub := hive.NewPubsub()

	//// check connectivity
	//_, err := conn.Do("PING")
	//if err != nil {
	//	log.Fatal("Can't connect to the Redis database")
	//}

	client := RedisClient{
		name,
		hive,
		conn,
		*pubsub,
		sync.Mutex{},
	}
	// launch goroutine for autoflush
	//client.Autoflush()
	//go func() {
	//	for {
	//		time.Sleep(200 * time.Millisecond)
	//		client.Lock()
	//		client.Conn.Flush()
	//		client.Unlock()
	//	}
	//}()
	return &client
}


func (client *RedisClient) Autoflush() {
	//
	go func() {
		for {
			time.Sleep(200 * time.Millisecond)
			client.Lock()
			client.Conn.Flush()
			client.Unlock()
		}
	}()
}



func (client *RedisClient) Publish(channel string, message interface{}) (int64,error) {
	client.Lock()
	n,err:= client.Conn.Do("PUBLISH", channel, message)
	if err != nil {
		log.Printf("PUBLISH Error: ",err)
		n=int64(0)
	}
	client.Unlock()
	return n.(int64),err
}

// flush publish connection
func (client *RedisClient) Flush() {
	client.Lock()
	client.Conn.Flush()
	client.Unlock()
}


func (client *RedisClient) Receive() RMessage {
	//
	switch message := client.PubSubConn.Receive().(type) {
	case redis.Subscription:
		//return RMessage{message.Kind, message.Channel, string(message.Count), ""}
		client.OnSubscription(&message)
	case redis.Message:
		return RMessage{"message", message.Channel, string(message.Data), ""}
	case redis.PMessage:
		return RMessage{"message", message.Channel, string(message.Data), message.Pattern}
	}
	return RMessage{}
}

func (client * RedisClient) Subscribe(channels ...interface{})  (err error) {
	// actor high level subscribe
	for _, channel := range channels {
		if strings.ContainsAny(channel.(string),"*") {
			client.PubSubConn.PSubscribe(channel)
		} else {
			client.PubSubConn.Subscribe(channel)
		}
	}
	return
}

func (client * RedisClient) Unsubscribe(channels ...interface{}) (err error) {

	// actor high level subscribe
	if len(channels) == 0 {
		// unsubcribe all
		client.PubSubConn.PUnsubscribe()
		client.PubSubConn.Unsubscribe()
	}
	for _, channel := range channels {
		if strings.ContainsAny(channel.(string),"*") {
			client.PubSubConn.PUnsubscribe(channel)
		} else {
			client.PubSubConn.Unsubscribe(channel)
		}
	}
	return
}

func (client * RedisClient) OnSubscription(subscription * redis.Subscription)  {
	//
	log.Printf("messenger [%s] has received subscription confirmation for channel [%s] ,count=%d",
		client.Name,subscription.Channel,subscription.Count)

}