package dnswatch_test



import (
	"testing"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	//"bitbucket.org/cocoon_bitbucket/dnstrack/dnswatch"

	"log"
	//"github.com/davecgh/go-spew/spew"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnswatch"
)

var (
	name = "hive"

	redis_url = "redis://192.168.99.100:6379/0"

	influxdb_url = "http://influxdb:8086"

	broker_available = false
)


func TestWatcher(t *testing.T) {


	hive := dnsbroker.NewHive(name,redis_url)


	err := hive.Check(false)
	if err != nil {
		broker_available = false
		log.Printf("no broker available: standalone mode\n")
	} else {
		broker_available = true
		log.Printf("broker [%s] available at %s\n" ,hive.Name, hive.Url)
	}

	writer := dnswatch.NewInfluxDbWriter(influxdb_url,"dns","dns")



	if broker_available == true {
		client := dnsbroker.NewRedisClient("me")
		client.Lock()
		client.Conn.Do("SET","dnswatch","starting")
		client.Unlock()
		client.Subscribe("dnsguard.packets")
		log.Printf("waiting for message on channel dnsguard.packets")
		for {
			rmessage := client.Receive()
			message:= rmessage.Data

			//spew.Dump(message)

			//log.Printf("received: \n%s\n ", message)
			if message != "" {

				typ := dnsbroker.DnsGuardQuestion{}
				data := typ.UnMarshal([]byte(message))
				//spew.Dump(data)
				log.Printf("%s,%s", data.QuestionName, data.EtldPlusOne)

				writer.AddDnsQuestion(*data)

			}
		}

	} else {

		log.Fatal("no broker available")
	}


}
