#!/usr/bin/env ash
set -ex

trap cleanup INT TERM

cleanup() {
### restore iptables to realease udp interception
#    iptables -D OUTPUT -i eth0 -p udp  -j NFQUEUE --queue-num 0
    iptables -D INPUT -i eth0 -p udp -j NFQUEUE --queue-num 0
    exit
}

### setup iptables to intercept udp traffic
iptables -A INPUT -i eth0 -p udp -j NFQUEUE --queue-num 0
#iptables -A OUTPUT -i eth0 -p udp  -j NFQUEUE --queue-num 0

### launch dnsguardian
#go run main.go
./dnsguardian
