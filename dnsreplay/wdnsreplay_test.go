
package dnsreplay_test

// Use tcpdump to create a test file
// tcpdump -w test.pcap
// or use the example above for writing pcap files



import (
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"testing"

	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsreplay"
	//"time"
)

//var (
//	pcapFile string = "../archive.test.pcap"
//	handle   *pcap.Handle
//	err      error
//)




func TestWDnsReplayer(t *testing.T) {



	// setup configuration singleton
	cnf := dnsbroker.NewConfig("./dnstrak.toml")

	// setup hive singleton for publisher
	dnsbroker.NewHive("hive",cnf.Redis.Url)


	// create a publisher
	publisher := dnsbroker.NewPubSubBroker()

	// create the player
	player := dnsreplay.NewWReplayer(pcapFile,publisher)

	player.Start()


	//time.Sleep(10 * time.Second)

	player.Stop()





}
