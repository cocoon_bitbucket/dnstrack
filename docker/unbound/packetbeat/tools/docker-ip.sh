#!/usr/bin/env bash

# get the ip of a container named  web

exec docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$@"