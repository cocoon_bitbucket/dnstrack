/*


	event["type"]
		dns : a dns query ( question + reply )

		dns_q:  a dns question ( no-reply )

		dns_dropped  a dns question that has been dropped ( reason , local_rules or blacklist )



 */



package dnsfilter

import (
	"log"
	//"os"
	//"os/signal"
	//"github.com/davecgh/go-spew/spew"
	"github.com/google/gopacket"
	//"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/layers"
	//"github.com/davecgh/go-spew/spew"

	"golang.org/x/net/publicsuffix"
	"strings"

	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
)


// a redis pubsub publisher
type Filter struct {

	counter uint
	publisher * dnsbroker.Broker
}


func NewFilter(publisher dnsbroker.Broker) Filter {


	log.Printf("create new filter\n")

	filter := Filter{0,&publisher}

	return filter
}



//func ( filter * Filter) Do( packet gopacket.Packet ) (map[string]interface{},error) {
func ( filter * Filter) Do( packet gopacket.Packet ) ( bool,error) {

	//
	// take a dns gopacket, dissect it and extract an 'event' dictionary
	//
	//    process a packet
	//      dissect it
	//      Check_local_rules() : check packet against local rules ( defined in local_check module)
	//		publish() : export packet to store
	//      Check_blacklist_rules
	//
	//    return true if packet is OK , and false if not



	event := make(map[string]interface{})
	event["_verdict"] = "PASS"
	event["udp_size"] = 0

	var (
		eth  layers.Ethernet
		ip4   layers.IPv4
		ip6 layers.IPv6
		tcp  layers.TCP
		udp  layers.UDP
		dns  layers.DNS
		body gopacket.Payload
	)

	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&eth,
		&ip4,
		&ip6,
		&tcp,
		&udp,
		&dns,
		&body)

	var foundLayerTypes []gopacket.LayerType
	err := parser.DecodeLayers(packet.Data(), &foundLayerTypes)
	if err != nil {
		log.Printf("[DecodeLayers error   ] %v %v", err, foundLayerTypes)
		// cannot decode packet: let it pass
		return true, nil
	}

	isDns := false

	for _, layer := range foundLayerTypes {
		switch layer {

		case layers.LayerTypeIPv4:
			event["SrcIP"] = ip4.SrcIP.String()
			event["DstIP"] = ip4.DstIP.String()
			event["dummy"] = ip4.Length
		case layers.LayerTypeIPv6:
			event["SrcIP"] = ip6.SrcIP.String()
			event["DstIP"] = ip6.DstIP.String()
		case layers.LayerTypeUDP:
			event["udp_size"] = udp.Length
		case layers.LayerTypeDNS:
			isDns = true
		}
	}

	if isDns == false {
		// not a dns request
		log.Printf("Not a dns Packet")
		// not a dns packet : let it pass
		return true, nil
	}

	//
	// we got a dns packet
	//
	log.Printf("Got a Dns packet")
	event["type"] = "dns"
	event["dns.id"] = dns.ID
	event["dns.op_code"] = dns.OpCode
	event["dns.response_code"] = dns.ResponseCode
	event["questions_count"] = dns.QDCount
	event["answers_count"] = dns.ANCount
	// feed question
	event["dns.question.name"] = ""
	for _, dnsQuestion := range dns.Questions {
		event["dns.question.name"] = string(dnsQuestion.Name)
		// only take the first question
		break
	}

	//if event["udp_size"] > 0 {
	//	event["dns.opt.udp_sizeudp_size"] = event["udp_size"]
	//}

	//event["dns.Questions"]= dns.Questions
	//event["dns.Answers"]= dns.Answers


	reply := dns.QR
	if reply == false {

		// this is a simple query (no answers)
		log.Printf("got a dns question  for %s ,(%d) \n", event["dns.question.name"],dns.ID,)



		//  check domain with remote check  (blacklist)
		// event["_verdict"] = guardian.Check_blacklist_rules(dns)
		event["_verdict"] = filter.remote_check(dns)

		if event["_verdict"] == "DROP" {
			event["type"] = "dns_dropped"
			event["drop.reason"] = "blacklisted_domain"
			//guardian.publisher.PublishDropped(rawPacket.ID,event,dns)
			filter.drop(1,event)
			log.Print("DROP Packet : domain is in blacklist")
			// packet is refused by remote check: drop it
			return false,nil
		}


		//
		// check packet with local rules
		//
		//event["_verdict"] = Check_local_rules(dns)
		event["_verdict"] = filter.local_check(dns)
		if event["_verdict"] == "DROP" {
			event["type"] = "dns_dropped"
			event["drop.reason"] = "break_rules"
			filter.drop(1,event)
			//guardian.publisher.PublishDropped(rawPacket.ID,event,dns)
			log.Print("DROP Packet according to local rules")
			// packet is refused by local_check: drop it
			return false,nil
		}

		// publish all questions
		for _, dnsQuestion := range dns.Questions {
			name := string(dnsQuestion.Name)
			event["dns.question.name"] = name
			//event["type"] = "dns_question"
			//event["dns.question.type"] = dnsQuestion.Type
			//event["dns.question.class"] = dnsQuestion.Class
			eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
			if err == nil {
				event["dns.question.etld_plus_one"] = eTLDPlusOne + "."
			} else {
				event["dns.question.etld_plus_one"] = "unknown."
			}
			//filter.publisher.PublishQuestion(1,event,dnsQuestion)
			filter.publish(1,event,dnsQuestion)
		}

		return true,nil

	} else {
		// this is a reply (question+reply)
		//log.Printf("got a dns reply  for %s ,(%d) \n", event["dns.question.name"],dns.ID,)
		// publish it
		//event["type"] = "dns"
		//guardian.publisher.PublishQuery(rawPacket.ID,event,dns)

	}
	// let it pass in any other case
	return true, err
}


//
// interface with publisher/broker
//
func ( filter * Filter) local_check( dns layers.DNS) string {

	// perform a local check  function Check_local_rules() is defined in local_check.go
	return Check_local_rules(dns)

}

func ( filter * Filter) remote_check( dns layers.DNS) string {

	// perform a local check  function Check_local_rules is defined in local_check.go
	//guardian.Check_blacklist_rules(dns)
	return filter.Check_blacklist_rules(dns)

}





func ( filter * Filter) publish( id int, event map[string]interface{}, question interface{} ) error {
	// publish a packet to the store
	//filter.publisher.PublishQuestion(1,event,dnsQuestion)

	// publish packet info via publisher
	log.Printf("publish  dns question (fake) for %s ,(%d) \n", event["dns.question.name"],id,)

	dnsbroker.ExportPacket( *filter.publisher, event)

	return nil

}

func ( filter * Filter) drop( id int, event map[string]interface{} ) error {
	//
	// tell  publisher we drop a packet
	//

	//guardian.publisher.PublishDropped(rawPacket.ID,event,dns)

	log.Printf("drop packet (fake) for %s ,(%d) \n", event["dns.question.name"],id)

	channel := publisher_name + "/" + "packets"

	dnsbroker.PublishEvent( *filter.publisher, "drop", event["dns.question.name"])

	//channel := publisher_name + "/" + "packets"

	//message := MakeDnsMessages(pktId,event,dns)

	//publisher.Publish(channel,event)

	log.Printf("publisher %s publish on channel %s",publisher_name,channel)


	return nil

}


func ( filter * Filter) isDomainInBlacklist( domain string ) bool {
	// ask broker if domain in blacklist
	return dnsbroker.IsDomainInBlacklist( *filter.publisher, domain )

}
