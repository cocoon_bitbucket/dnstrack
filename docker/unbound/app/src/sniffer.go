/*

    see: https://github.com/dvas0004/GolangSimpleDnsSniffer
*/

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	//"strconv"
	"sync"
	//"time"

	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"golang.org/x/net/publicsuffix"
	"strings"
	influx "github.com/influxdata/influxdb/client/v2"
	//"github.com/influxdata/influxdb/client"
	//"github.com/influxdata/influxdb/client"
	//"github.com/influxdata/influxdb/client"
	"time"
)


const (
	MyDB = "dns"
	username = "dns"
	password = "dns"
)

var  (

	influxdb_url = "http://influxdb:8086"
	influxdb_cnx influx.Client = nil
	influxdb_bp  influx.BatchPoints = nil
	influxdb_period time.Duration= 5
	influxdb_time time.Time = time.Now()
	//message = dnsStore.DnsMsg{}
)

var (
	devName 	string
	es_index 	string
	es_docType 	string
	es_server	string
	err      	error
	handle   	*pcap.Handle
	InetAddr 	string
	SrcIP    	string
	DstIP    	string
)

type DnsResponse map[string]interface{}

type DnsMsg struct {
	Timestamp       string
	SourceIP        string   // tag
	DestinationIP   string
	DnsQuery        string   // tag
	DnsAnswer       []string
	DnsAnswerTTL    []string
	NumberOfAnswers string
	DnsResponseCode string   // tag
	DnsOpCode       string   // tag
}

func sendToElastic(dnsMsg DnsMsg, wg *sync.WaitGroup) {
	defer wg.Done()

	var jsonMsg, jsonErr = json.Marshal(dnsMsg)
	if jsonErr != nil {
		panic(jsonErr)
	}

	// getting ready for elasticsearch
	request, reqErr := http.NewRequest("POST", "http://"+es_server+":9200/"+es_index+"/"+es_docType,
		bytes.NewBuffer(jsonMsg))
	if reqErr != nil {
		panic(reqErr)
	}

	client := &http.Client{}
	resp, elErr := client.Do(request)

	if elErr != nil {
		panic(elErr)
	}

	defer resp.Body.Close()

}

func sendToInfluxdb(dnsMsg DnsResponse, wg *sync.WaitGroup) {
	defer wg.Done()

	//var jsonMsg, jsonErr = json.Marshal(dnsMsg)
	//if jsonErr != nil {
	//	panic(jsonErr)
	//}
	//
	//// getting ready for elasticsearch
	//request, reqErr := http.NewRequest("POST", "http://"+es_server+":9200/"+es_index+"/"+es_docType,
	//	bytes.NewBuffer(jsonMsg))
	//if reqErr != nil {
	//	panic(reqErr)
	//}
	//
	//client := &http.Client{}
	//resp, elErr := client.Do(request)
	//
	//if elErr != nil {
	//	panic(elErr)
	//}
	//
	//defer resp.Body.Close()

	// Create a new HTTPClient
	if influxdb_cnx == nil {
		influxdb_cnx, err = influx.NewHTTPClient(influx.HTTPConfig{
			Addr:     influxdb_url,
			Username: username,
			Password: password,
		})
		if err != nil {
			log.Fatal(err)
		}

		// Create a new point batch
		if influxdb_bp == nil {
			influxdb_bp, err = influx.NewBatchPoints(influx.BatchPointsConfig{
				Database:  MyDB,
				Precision: "ms",
			})
			if err != nil {
				log.Fatal(err)
			}
		}
		influxdb_time = time.Now()
	}

	//
	// Create a point and add to batch
	tags := map[string]string{
		"question" : strings.ToLower(dnsMsg["question"].(string)) ,
		"etld_plus_one": strings.ToLower(dnsMsg["etld_plus_one"].(string)) ,
	}
	fields := map[string]interface{}{
		"Id":   dnsMsg["Id"],
		"opCode" : dnsMsg["opCode"],
		"responseCode": dnsMsg["responseCode"],
	}

	pt, err := influx.NewPoint("dns_response", tags, fields, time.Now())
	if err != nil {
		log.Fatal(err)
	}
	influxdb_bp.AddPoint(pt)


	// Write the batch
	now := time.Now()
	diff := now.Sub(influxdb_time)
	if diff.Seconds() > float64(influxdb_period) {
		if err := influxdb_cnx.Write(influxdb_bp); err != nil {
			log.Fatal(err)
		}
		influxdb_time = time.Now()
	}

}







func main() {

	////// CONFIG SECTION: REVIEW THESE BEFORE USING

	// select a device to listen on
	//windows example
	//devName = "\\Device\\NPF_{9CA25EBF-B3D8-4FD0-90A6-070A16A7F2B4}"
	//linux example
	devName = "eth0"

	// define an elasticsearch server to send to
	//es_server = "192.168.10.15"
	es_server = "elasticsearch"
	// define an elasticsearch index to send to
	es_index = "dns_index"
	es_docType = "syslog"

	// END CONFIG SECTION


	var eth layers.Ethernet
	var ip4 layers.IPv4
	var ip6 layers.IPv6
	var tcp layers.TCP
	var udp layers.UDP
	var dns layers.DNS

	var payload gopacket.Payload

	wg := new(sync.WaitGroup)


	// Find all devices
	devices, devErr := pcap.FindAllDevs()
	if devErr != nil {
		log.Fatal(devErr)
	}

	// Print device information
	fmt.Println("Devices found:")
	for _, device := range devices {
		fmt.Println("\nName: ", device.Name)
		fmt.Println("Description: ", device.Description)
		fmt.Println("Devices addresses: ", device.Description)
		for _, address := range device.Addresses {
			if device.Name == devName {
				InetAddr = address.IP.String()
				break
			}
			fmt.Println("- IP address: ", address.IP)
			fmt.Println("- Subnet mask: ", address.Netmask)
		}
	}

	// // Create DNSQuery index
	// _, elErr = client.CreateIndex("dns_query").Do()
	// if elErr != nil {
	//     // Handle error
	//     panic(elErr)
	// }

	// Open device
	handle, err = pcap.OpenLive(devName, 1600, false, pcap.BlockForever)
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()

	// Set filter
	//addr := InetAddr
	//addr := "192.168.1.151"
	//addr := "192.168.1.58"

	//var filter string = "udp and port 5053 and src host " + addr

	//var filter string = "udp and port 5053"
	var filter string = "udp"

	fmt.Println("    Filter: ", filter)
	err := handle.SetBPFFilter(filter)
	if err != nil {
		log.Fatal(err)
	}

	parser := gopacket.NewDecodingLayerParser(layers.LayerTypeEthernet, &eth, &ip4, &ip6, &tcp, &udp, &dns, &payload)

	decodedLayers := make([]gopacket.LayerType, 0, 10)
	for {
		data, _, err := handle.ReadPacketData()
		if err != nil {
			fmt.Println("Error reading packet data: ", err)
			continue
		}

		Msg := make(DnsResponse)

		err = parser.DecodeLayers(data, &decodedLayers)
		for _, typ := range decodedLayers {
			switch typ {
			case layers.LayerTypeIPv4:
				SrcIP = ip4.SrcIP.String()
				DstIP = ip4.DstIP.String()
			case layers.LayerTypeIPv6:
				SrcIP = ip6.SrcIP.String()
				DstIP = ip6.DstIP.String()
			case layers.LayerTypeDNS:
				Msg["Id"] = dns.ID
				Msg["opCode"] = int(dns.OpCode)
				Msg["responseCode"] = int(dns.ResponseCode)

				dnsId := dns.ID
				dnsOpCode := int(dns.OpCode)
				dnsResponseCode := int(dns.ResponseCode)
				dnsANCount := int(dns.ANCount)

				//if dns.QR == false {
				//	//
				//	// this is a DNS QUERY
				//	//
				//	fmt.Println("==========    DNS QUERY Only Detected  QR is false  =========")
				//	fmt.Println("dns_id: " ,dnsId)
				//	fmt.Println("opcode: " ,dnsOpCode)
				//	fmt.Println("response_code: " ,dnsResponseCode)
				//	fmt.Println("answer_count: " ,dnsANCount)
				//	for _, dnsQuestion := range dns.Questions {
				//
				//		//t := time.Now()
				//		//timestamp := t.Format(time.RFC3339)
				//		name:= string(dnsQuestion.Name)
				//		fmt.Println("Question: ", name)
				//		eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
				//		if err == nil {
				//			fmt.Println("etld_plus_one" ,eTLDPlusOne , ".")
				//		}
				//		fmt.Println("Endpoints: ", SrcIP, DstIP)
				//	}

				if dns.QR == true {
					//
					// this is a DNS RESPONSE
					//

					fmt.Println("+++++  DNS QUERY/RESPONSE  QR is True +++++++")
					fmt.Println("dns_id: " ,dnsId)
					fmt.Println("opcode: " ,dnsOpCode)
					fmt.Println("response_code: " ,dnsResponseCode)
					fmt.Println("answer_count: " ,dnsANCount)
					for _, dnsQuestion := range dns.Questions {

						//t := time.Now()
						//timestamp := t.Format(time.RFC3339)
						name:= string(dnsQuestion.Name)
						fmt.Println("Question: ", name)
						Msg["question"] = name
						eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
						if err == nil {
							fmt.Println("etld_plus_one" ,eTLDPlusOne , ".")
							Msg["etld_plus_one"] = eTLDPlusOne
						} else {
							fmt.Println("CANNOT get etld_plus_one for ",name)
							Msg["etld_plus_one"]= "UNKNOWN"
						}

						fmt.Println("Endpoints: ", SrcIP, DstIP)
						if dnsANCount > 0 {
							// there is answers
							for _, dnsAnswer := range dns.Answers {
								fmt.Println("answer_ttl", dnsAnswer.TTL)
								if dnsAnswer.IP.String() != "<nil>" {
									fmt.Println("answer_ip", dnsAnswer.IP.String())
								}
							}

						}
						// write message
						wg.Add(1)
						sendToInfluxdb(Msg,wg)


					}
				}

			}
		}

		if err != nil {
			fmt.Println("  Error encountered:", err)
		}
	}
}

