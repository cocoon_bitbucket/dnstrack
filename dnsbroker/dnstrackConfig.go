package dnsbroker



import (
	//"fmt"
	//"time"
	//
	"github.com/BurntSushi/toml"
	"log"
)

var (
	configFilename string = "dnstrack.toml"
	dnstrackConfig  * DnstrackConfig
)


type DnstrackConfig struct {

	Version string

	// prefix to store blacklisted domains  Eg  blacklist:piratedns.io.
	BlacklistPrefix string `toml:"blacklist_prefix"`
	// time to live for a blacklisted domain
	BlacklistTTL uint `toml:"blacklist_ttl"`
	// a list of domains
	WhiteList []string

	Redis RedisConfig
	Influxdb InfluxDbConfig

	Dnswatch DnswatchConfig

	DnsGuard DnsguardConfig

}

type PropertiesConfig struct {
	Properties map[string]string
}


type RedisConfig struct {

	Url string

}


type InfluxDbConfig struct {

	Url string
	Db  string
	Username  string
	Password string

	FlushDelay uint  `toml:"flush_delay"`

}

type DnswatchConfig struct {
	// time interval in s between 2 scan of the database to find tunnels
	ScanInterval uint  `toml:"scan_interval"`
	// duration in s of the scan to find tunnels
	ScanPeriod uint  `toml:"scan_period"`
	// limit of unique hostname for a domain to detect a tunnel over the scan_period
	ScanLimit uint  `toml:"scan_limit"`
}

type DnsguardConfig struct {

	Name string

	QuestionChannel string `toml:"question_channel"`
	DropChannel string `toml:"drop_channel"`
	QueryChannel string `toml:"query_channel"`

	ActiveChannels []string  `toml:"active_channels"`

}



//// create a new Config
//func NewConfig() * DnstrackConfig {
//
//
//	dnstrackOnce.Do(func() {
//		// create a new config singleton
//		if _, err := toml.DecodeFile(configFilename, &dnstrackConfig); err != nil {
//			log.Fatal(err)
//		}
//
//	})
//
//	return dnstrackConfig
//}

// create a new Config
func NewConfig(configFileName string) * DnstrackConfig {


	if dnstrackConfig != nil {
		log.Fatal("configuration already instanciated")

	}

	if _, err := toml.DecodeFile(configFilename, &dnstrackConfig); err != nil {
		log.Fatal(err)
	}

	return dnstrackConfig
}

func GetConfig() * DnstrackConfig {

	if dnstrackConfig == nil {
		log.Fatal("No Config has been instanciated with NewConfig(filename)")
	}

	// create a new config
	var config DnstrackConfig
	// copy the original config
	config = * dnstrackConfig

	// return the copy
	return &config
	//return dnstrackConfig
}

