/*

	an ambassador is an a special emiter to publish messages on a foreign hive

	 it subscribes to local hive on channel  ambassador-of-<foreign>.*
	 and publish  the message on the foreign hive


	examples
	a local hive    "local" on url "redis://127.0.0.1:6359/0"
	a foreign hive  "foreign" on url "redis://192.168.99.100:6359/1"

	the local client "here" publish a message to  "remote@foreign"

	since the domain "foreign" is not local the publish script publish it to "ambassador-of-foreign.remote

	the ambassador receive the message and re-publish it to the remote hive on channel "remote"

	then the remote client on hive "foreign" receive the message

 */


package dnsbroker

import (
	"sync"
	//"time"
)


var (

	ambassador_prefix= "ambassador-for-"
	embassy_prefix =  "embassy-of-"
)


type Ambassador struct {

	RedisClient
	ForeignName string
	ForeignUlr string
	foreignClient RedisClient

}


func NewAmbassador( foreignNname string,foreignUrl string) *Ambassador {

	// set local name: ambassador-foreign
	localName := ambassador_prefix + foreignNname
	localSubscriptionChannel := localName + ".*"

	client := NewRedisClient(localName)

	//hive := GetHive()
	//conn := hive.NewConn()
	//pubsub := hive.NewPubsub()
	//
	//
	//client := RedisClient{
	//	localName,
	//	hive,
	//	conn,
	//	*pubsub,
	//	sync.Mutex{},
	//}
	// subscribe to local channel ambassador-of-<foreignName>
	client.Subscribe(localName)
	client.Subscribe(localSubscriptionChannel)  // ambassador-of-<foreign>.*


	// foreign hive
	foreignHive := NewFreeHive(foreignNname,foreignUrl)
	foreignConn := foreignHive.NewConn()
	foreignPubsub := foreignHive.NewPubsub()
	embassyName := "embassy-of-" + client.hive.Name  // ambassy-of-local
	foreignClient := RedisClient{
		embassyName ,
		foreignHive,
		foreignConn,
		* foreignPubsub,
		sync.Mutex{},
	}

	// launch goroutine for autoflush
	ambassador := Ambassador{
		*client,
		foreignNname,
		foreignUrl,
		foreignClient,
	}

	return &ambassador
}


func (ambassador *Ambassador) Publish(channel string, message interface{}) {
	// publish to foreign channel
	ambassador.foreignClient.Lock()
	ambassador.foreignClient.Conn.Send("PUBLISH", channel, message)
	ambassador.foreignClient.Unlock()
}

func (ambassador *Ambassador) OnMessage( message RMessage) {
	// emit message to foreign hive
	// TODO: extract the channel frome message.Channel  ambassador-for-foreign.remote => remote
	ambassador.foreignClient.Publish(message.Channel,message.Data)
}


func (ambassador *Ambassador) Run() {

	go func() {
		for {
			message := ambassador.Receive()
			ambassador.OnMessage(message)
		}
	}()
}




