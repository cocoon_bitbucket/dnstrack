/*
	export a dns message as a json string

 */
package dnsguard


//import (
//	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
//	"github.com/google/gopacket/layers"
//	"strings"
//	"encoding/json"
//	"fmt"
//	"log"
//)



import (
	"github.com/google/gopacket/layers"
	"golang.org/x/net/publicsuffix"
	"strings"
	"encoding/json"
	"fmt"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"log"
	//"github.com/davecgh/go-spew/spew"
)

var(
	publisher_name = "dnsguard"
)



// a redis pubsub publisher
type Publisher struct {

	dnsbroker.RedisClient
	connected bool

}


func NewPublisher() * Publisher {
	// create a new publisher
	// assume a Hive singleton has been created
	client := dnsbroker.NewRedisClient(publisher_name)

	publisher := Publisher{*client,false}


	return &publisher
}

func (publisher *Publisher) watch_dog() {

	// check connectivity
	publisher.Lock()
	_, err := publisher.Conn.Do("PING")
	publisher.Unlock()
	if err != nil {
		publisher.connected = true
	} else {
		publisher.connected = false
	}
}

func (publisher *Publisher) Trace (channel string,message []byte) {

	log.Printf("----------------> trace: %s" ,message)

	publisher.Lock()
	_,err:= publisher.Do("LPUSH" , channel ,message)
	publisher.Unlock()
	if err != nil {
		log.Fatal("LPUSH Error: ",err)
	}

}




func (publisher *Publisher) PublishQuery(pktId uint32, event map[string]interface{} ,dns  layers.DNS){
	// publish a complete query (question + reply )
	log.Printf("Publish a complete Dns Query\n")
	channel := publisher_name + "/" + "packets"

	//message := MakeDnsMessages(pktId,event,dns)

	//publisher.Publish(channel,message)

	log.Printf("publisher %s publish on channel %s",publisher_name,channel)
}



func (publisher *Publisher) PublishQuestion(pktId uint32, event map[string]interface{} ,dnsQuestion  layers.DNSQuestion){
	// publish questions to channel dnsguard/packets
	log.Printf("Publish Dns Question (%s,%s) on channel [dnsguard.packets]\n",
		event["dns.question.etld_plus_one"],event["dns.question.name"])


	//spew.Dump(dnsQuestion)
	channel := publisher_name + ".packets"

	//domain := []byte(event["dns.question.etld_plus_one"])
	domain := event["dns.question.etld_plus_one"].(string)

	data := dnsbroker.NewDnsGuardQuestion2(
		event["dns.id"].(uint16),
		//event["dns.question.etld_plus_one"].([]byte),
		domain,
		dnsQuestion,
	)
	//spew.Dump(data)

	message := data.Marshal()
	//println("message: ",message)
	//message := data

	//publisher.Trace(channel,message)
	_,err:= publisher.Publish(channel,message)
	if err != nil {
		//log.Fatal("PUBLISH ERROR: ",err)
	}

	// tempo to slow down
	//time.Sleep(10 * time.Millisecond)


	//log.Printf("publisher %s publish on channel %s",publisher_name,channel)
	//spew.Dump(event)

}



func (publisher *Publisher) PublishDropped(pktId uint32, event map[string]interface{} ,dns  layers.DNS){
	// publish a dropped questions
	log.Printf("Publish a dropped Dns Question\n")

	channel := publisher_name + "/" + "packets"

	//message := MakeDnsMessages(pktId,event,dns)

	//publisher.Publish(channel,event)

	log.Printf("publisher %s publish on channel %s",publisher_name,channel)
}




func MakeDnsMessages(pktId uint32, event map[string]interface{} ,dns  layers.DNS) []byte {

	event["dns.qr"] = dns.QR

	event["dns.id"] = dns.ID
	event["dns.op_code"] = dns.OpCode
	event["dns.response_code"] = dns.ResponseCode
	event["questions_count"] = dns.QDCount
	event["answers_count"] = dns.ANCount

	//event["dns.Questions"]= dns.Questions
	//event["dns.Answers"]= dns.Answers

	for _, dnsQuestion := range dns.Questions {
		//name:= fmt.Sprintf("")
		name := strings.ToLower(string(dnsQuestion.Name))
		event["dns.question.name"] = name
		eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
		if err == nil {
			event["dns.question.etld_plus_one"] = eTLDPlusOne + "."
		}
		event["dns.question.type"] = dnsQuestion.Type
		event["dns.question.class"] = dnsQuestion.Class
		// take only the first question
		break
	}

	//spew.Dump(event)

	message, _ := json.Marshal(event)
	fmt.Println(string(message))




	return message

}

