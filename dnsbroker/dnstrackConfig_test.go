package dnsbroker_test

import(

	"testing"
	"github.com/BurntSushi/toml"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"fmt"
	"github.com/davecgh/go-spew/spew"

	"github.com/stretchr/testify/assert"
)



func TestLoadConfig(t *testing.T) {


	var config dnsbroker.DnstrackConfig
	if _, err := toml.DecodeFile("dnstrack.toml", &config); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("version: %s\n", config.Version)

	fmt.Printf("blacklist_prefix: %s\n", config.BlacklistPrefix)
	fmt.Printf("blacklist_ttl: %d\n", config.BlacklistTTL)
	spew.Dump(config.WhiteList)


	fmt.Printf("redis.url: %s\n", config.Redis.Url)

	fmt.Printf("influxdb.url: %s\n", config.Influxdb.Url)
	fmt.Printf("influxdb.db: %s\n", config.Influxdb.Db)
	fmt.Printf("influxdb.username: %s\n", config.Influxdb.Username)
	fmt.Printf("influxdb.password: %s\n", config.Influxdb.Password)
	fmt.Printf("influxdb.flush_delay: %d\n", config.Influxdb.FlushDelay)

	fmt.Printf("dnswatch.scan_interval: %d\n", config.Dnswatch.ScanInterval)
	fmt.Printf("dnswatch.scan_period: %d\n", config.Dnswatch.ScanPeriod)
	fmt.Printf("dnswatch.scan_limit: %d\n", config.Dnswatch.ScanLimit)

	fmt.Printf("dnsguard.name: %s\n", config.DnsGuard.Name)
	fmt.Printf("dnsguard.question_channel: %s\n", config.DnsGuard.QuestionChannel)
	fmt.Printf("dnsguard.drop_channel: %s\n", config.DnsGuard.DropChannel)
	fmt.Printf("dnsguard.query_channel: %s\n", config.DnsGuard.QueryChannel)

	spew.Dump(config.DnsGuard.ActiveChannels)
}



func TestNewConfig(t *testing.T) {


	// create new config
	conf := dnsbroker.NewConfig("./dnstrack.toml")
	fmt.Printf("version: %s\n", conf.Version)
	spew.Dump(conf)

	// get a copy of the config
	conf2 := dnsbroker.GetConfig()
	fmt.Printf("version: %s\n", conf2.Version)
	spew.Dump(conf2)
	assert.Equal(t,conf,conf2)


	// modify the configuration copy
	conf2.Version="0.2"

	// the original configuration should not be affected
	assert.NotEqual(t,conf.Version,conf2.Version)
	fmt.Printf("version: %s\n", conf.Version)



}


