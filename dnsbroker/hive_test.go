package dnsbroker_test



import (
	"testing"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
)


func TestGetHive(t *testing.T) {
	//

	name := "hive"
	url := "redis://localhost:6379/1"


	dnsbroker.ResetHive()


	h := dnsbroker.NewHive(name,url)

	h2 := dnsbroker.GetHive()

	println(h==h2)

	h2.Close()

	dnsbroker.ResetHive()
	h3 := dnsbroker.NewHive(name,url)

	println(h3)



}




func TestCreateHive(t *testing.T) {
	//

	name := "hive"
	url := "redis://localhost:6379/1"

	dnsbroker.ResetHive()

	h := dnsbroker.NewHive(name,url)

	h.Check(false)
	//var err error

	h.Close()

}

func TestCreateHiveTwice(t *testing.T) {
	//

	name := "hive"
	url := "redis://localhost:6379/1"
	//url2:= "redis://localhost:6379/2"


	dnsbroker.ResetHive()

	h := dnsbroker.NewHive(name,url)

	//h2 := dnsbroker.NewHive(name,url2)

	h.Check(false)
	//h2.Check(false)
	//var err error

}


