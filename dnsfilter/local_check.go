package dnsfilter

import (

	"github.com/google/gopacket/layers"
	"log"
)

func Check_local_rules(dns layers.DNS) string {

	verdict:= "PASS"
	// check dns event with local rules
	for _, dnsQuestion := range dns.Questions {
		//name:= fmt.Sprintf("")
		name := string(dnsQuestion.Name)
		typ := dnsQuestion.Type
		//other := dnsQuestion.Class
		println("dns question type: ",typ.String(),"\n")
		if typ == 16 {
			log.Println("DNS QUESTION OF TYPE TXT ==> DROP :",name,"\n")
			verdict = "DROP"
			break
		}
	}
	return verdict
}



