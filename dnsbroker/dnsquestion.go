/*

	describe the format of the message



	from layers

	type DNSOpCode uint8
	type DNSType uint16
	type DNSClass uint16

	// DNSQuestion wraps a single request (question) within a DNS query.
	type DNSQuestion struct {
		Name  []byte
		Type  DNSType     //
		Class DNSClass
	}





 */
package dnsbroker

import (

	"encoding/json"
	"github.com/google/gopacket/layers"

	"fmt"
	//"log"
)


//type question struct {
//
//	Name  []byte		`json:"dns.question.name"`
//	Type  layers.DNSType	`json:"dns.question.type"`
//	Class layers.DNSClass	`json:"dns.question.class"`
//
//}


// DNS contains data from a single Domain Name Service packet.
type DnsGuardQuestion struct {
	//BaseLayer

	// Header fields
	ID     uint16	`json:"dns.id"`
	QR     bool		`json:"dns.qr"`
	OpCode layers.DNSOpCode  `json:"dns.op_code"`

	AA bool  	`json:"dns.flags.authoritative"` // Authoritative answer
	TC bool  	`json:"dns.flags.truncated_response"`	// Truncated
	RD bool  	`json:"dns.flags.recursion_desired"`   // Recursion desired
	RA bool  	`json:"dns.flags.recursion_available"`	// Recursion available
	Z  uint8 // Reserved for future use

	QDCount      uint16 	`json:"dns.questions_count"`	// Number of questions to expect
	ANCount      uint16 	`json:"dns.answers_count"`	// Number of answers to expect
	NSCount      uint16 	`json:"dns.authorities_count"`	// Number of authorities to expect
	ARCount      uint16 	`json:"dns.additionals_count"`	// Number of additional records to expect

	// Entries
	//layers.DNSQuestion
	QuestionName []byte				`json:"dns.question.name"`
	QuestionType layers.DNSType		`json:"dns.question.type"`
	QuestionClass layers.DNSClass	`json:"dns.question.class"`

	EtldPlusOne []byte	`json:"dns.question.etld_plus_one"`



	// buffer for doing name decoding.  We use a single reusable buffer to avoid
	// name decoding on a single object via multiple DecodeFromBytes calls
	// requiring constant allocation of small byte slices.
	//buffer []byte
}


func NewDnsGuardQuestion( id uint16,domain []byte,name []byte, Type layers.DNSType,Class layers.DNSClass ) * DnsGuardQuestion {


	//q := layers.DNSQuestion{ name,Type,Class}
	//q := question{ name,Type,Class}


	gq :=  DnsGuardQuestion {
		id,
		false,
		0,
		false,
		false,
		false,
		false,
		0,
		1,
		0,
		0,
		0,
		name,
		Type,
		Class,
		domain,

	}

	return &gq
}

func NewDnsGuardQuestion2( id uint16,domain string, question layers.DNSQuestion ) * DnsGuardQuestion {

	//log.Printf("NewDnsguard (%d,%s,%s,%d,%d)",id,string(domain),string(question.Name),
	//	uint8(question.Type),uint8(question.Class))


	gq :=  DnsGuardQuestion {
		id,
		false,
		0,
		false,
		false,
		false,
		false,
		0,
		1,
		0,
		0,
		0,
		question.Name,
		question.Type,
		question.Class,
		[]byte(domain),

	}

	return &gq
}


func ( question * DnsGuardQuestion) Marshal() []byte{


	b, err := json.Marshal( question)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println(string(b))
	return b

}

//func Unmarshal(data []byte, ) error {

func ( question * DnsGuardQuestion) UnMarshal(data []byte) * DnsGuardQuestion {

	ds := DnsGuardQuestion{}
	err := json.Unmarshal(data, &ds)
	if err != nil {
		fmt.Println(err)
	}
	return &ds

}
