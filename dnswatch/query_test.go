package dnswatch_test


import (
	//"../dnsStore"
	"log"
	"testing"
	"github.com/influxdata/influxdb/client/v2"
	//"fmt"

	"time"
	"fmt"
	//"github.com/davecgh/go-spew/spew"
)

const (
	MyDB = "dns"
	username = "dns"
	password = "dns"
)


var (

	period_start time.Time
)


// queryDB convenience function to query the database
func queryDB(clnt client.Client, cmd string) (res []client.Result, err error) {
	q := client.Query{
		Command:  cmd,
		Database: MyDB,
	}
	if response, err := clnt.Query(q); err == nil {
		if response.Error() != nil {
			return res, response.Error()
		}
		res = response.Results
	} else {
		return res, err
	}
	return res, nil
}





func TestQuery(*testing.T) {
	// Create a new HTTPClient
	clnt, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     influxdb_url,
		Username: username,
		Password: password,
	})
	if err != nil {
		log.Fatal(err)
	}

	measurement := "dns-request"
	field := "dns.id"

	q := fmt.Sprintf("SELECT count(%s) FROM \"%s\"", field, measurement)
	res, err := queryDB(clnt, q)
	if err != nil {
		log.Fatal(err)
	}
	count := res[0].Series[0].Values[0][1]
	log.Printf("Found a total of %v records\n", count)


	q2 := fmt.Sprintf( `SELECT count("dns.id") AS "count_dns.id" FROM "dns"."autogen"."dns-request" WHERE time > now() - 1h GROUP BY *`)

	//q2 := "SELECT count(\"Id\") AS \"count_Id\" FROM \"dns\".\"autogen\".\"dns_response\" WHERE time > now() - 1h GROUP BY *"


	//q2 := fmt.Sprintf("SELECT count(%s) FROM %s", field, measurement)
	res2, err := queryDB(clnt, q2)
	if err != nil {
		log.Fatal(err)
	}

	last_domain := ""
	domains := make(map[string]int)

	for _,v := range res2[0].Series {
		//spew.Dump(v)

		domain := v.Tags["dns.question.etld_plus_one"]
		//name   := v.Tags["question"]

		if domain != last_domain {
			domains[domain]= 1
			last_domain= domain
		} else {
			domains[domain]= domains[domain] + 1
		}

		//println(domain,"->",name)

	}

	println("-----------")
	for k,v :=range domains {
		if v > 5 {
			println("domain: ", k, " has ", v, " names")
		}
	}

	//count2 := re2s[0].Series[0].Values[0][1]
	//log.Printf("Found a total of %v records\n", count2)

	//spew.Dump(res2)



}



