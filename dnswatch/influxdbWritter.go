package dnswatch



import (
//"../dnsStore"
"log"
"time"
influx "github.com/influxdata/influxdb/client/v2"
//"fmt"
//"math/rand"
//	"github.com/influxdata/influxdb/client"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"sync"
	//"github.com/influxdata/influxdb/client"
	//"github.com/golang/protobuf/ptypes/duration"
)

const (
	MyDB = "dns"
	username = "dns"
	password = "dns"
)

//type DnsQuestionTags map[string]string


var  (

	influxdb_url = "http://influxdb:8086"
	//message = dnsStore.DnsMsg{}

	influxdb_flush_delay = 500


	DnsQuestionTags = []string {
		"type",
		"dns.question.name" ,
		"dns.question.type",
		"dns.question.etld_plus_one",

	}



)


// a redis pubsub publisher
type InfluxDbWriter struct {

	influx.Client

	bp influx.BatchPoints

	sync.Mutex


}


func NewInfluxDbWriter(Addr string,username string,password string) * InfluxDbWriter {

	// Create a new HTTPClient
	c, err := influx.NewHTTPClient(influx.HTTPConfig{
		Addr:     influxdb_url,
		Username: username,
		Password: password,
	})
	if err != nil {
		log.Fatal(err)
	}

	writer := InfluxDbWriter{c,nil,sync.Mutex{}}
	writer.init_bp()
	writer.autoflush()

	return &writer

}

func ( writer * InfluxDbWriter) init_bp() {
	bp, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database:  "dns",
		Precision: "us",
	})
	if err != nil {
		log.Fatal(err)
	}
	writer.bp = bp

}

func (writer * InfluxDbWriter) autoflush() {
	//
	go func() {
		for {
			time.Sleep( 500 * time.Millisecond)
			l := len(writer.bp.Points())
			if l  > 0 {
				log.Printf("points length: %d ",uint(l))
				//log.Printf("writer autoflush set lock")
				writer.Lock()
				// write th bp
				if err := writer.Write(writer.bp); err != nil {
					writer.Unlock()
					log.Fatal(err)
				}
				//log.Printf("writer autoflush release lock")
				writer.init_bp()
				writer.Unlock()

			} else {
				//log.Printf("autoflush: no points to flush")
			}

		}
	}()
}




func ( writer * InfluxDbWriter) Close() {

}



func ( writer * InfluxDbWriter) AddDnsQuestion( question dnsbroker.DnsGuardQuestion) {

	//
	//  tags :   type dns.question.type dns.question.name dns.question.etld_plus_one dns.question.class
	//  fields: dns.id udp

	tags := make(map [string]string)
	fields := 	make(map[string]interface{})

	tags["type"] = "dns_question"
	tags["dns.question.name"]= string(question.QuestionName)
	tags["dns.question.etld_plus_one"]= string(question.EtldPlusOne)
	tags["dns.question.type"] = string(question.QuestionType)
	tags["dns.question.class"] = string(question.QuestionClass)

	fields["dns.id"] = uint(question.ID)

	pt, err := influx.NewPoint(
		"dns-request",
		tags,
		fields,
		time.Now(),
	)
	if err != nil {
		log.Fatal(err)
	}
	//log.Printf("writer AddDnsQuestion set lock")
	writer.Lock()
	writer.bp.AddPoint(pt)

	// write th bp
	//if err := writer.Write(writer.bp); err != nil {
	//	log.Fatal(err)
	//}
	//log.Printf("writer AddDnsQuestion release lock")
	writer.Unlock()

}



//// Create a new point batch
//bp, err := client.NewBatchPoints(client.BatchPointsConfig{
//Database:  MyDB,
//Precision: "s",
//})
//if err != nil {
//log.Fatal(err)
//}



//func writePoints(clnt client.Client) {
//	sampleSize := 1000
//
//	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
//		Database:  "dns",
//		Precision: "us",
//	})
//	if err != nil {
//		log.Fatal(err)
//	}
//
//	rand.Seed(time.Now().UnixNano())
//	for i := 0; i < sampleSize; i++ {
//		regions := []string{"us-west1", "us-west2", "us-west3", "us-east1"}
//		tags := map[string]string{
//			"cpu":    "cpu-total",
//			"host":   fmt.Sprintf("host%d", rand.Intn(1000)),
//			"region": regions[rand.Intn(len(regions))],
//		}
//
//		idle := rand.Float64() * 100.0
//		fields := map[string]interface{}{
//			"idle": idle,
//			"busy": 100.0 - idle,
//		}
//
//		pt, err := client.NewPoint(
//			"cpu_usage",
//			tags,
//			fields,
//			time.Now(),
//		)
//		if err != nil {
//			log.Fatal(err)
//		}
//		bp.AddPoint(pt)
//	}
//
//	if err := clnt.Write(bp); err != nil {
//		log.Fatal(err)
//	}
//}

