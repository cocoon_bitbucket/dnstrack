package dnsreplay_test

// Use tcpdump to create a test file
// tcpdump -w test.pcap
// or use the example above for writing pcap files



import (
	"fmt"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsfilter"
	"log"
	"testing"

	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsreplay"
)

//var (
//	pcapFile string = "../archive.test.pcap"
//	handle   *pcap.Handle
//	err      error
//)


func TestDnsReplay(t *testing.T) {



	// setup configuration singleton
	cnf :=dnsbroker.NewConfig("./dnstrak.toml")

	// setup hive singleton for publisher
	dnsbroker.NewHive("hive",cnf.Redis.Url)

	publisher := dnsbroker.NewSimpleBroker()

	// create the filter
	filter := dnsfilter.NewFilter( publisher)




	// Open file instead of device

	handle, err = pcap.OpenOffline(pcapFile)
	if err != nil { log.Fatal(err) }
	defer handle.Close()
	log.Printf("replay pcap file %s \n",pcapFile)
	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
	for packet := range packetSource.Packets() {
		fmt.Println(packet)
		filter.Do(packet)
	}
	log.Printf("end of pcap file %s \n",pcapFile)

}

func TestDnsReplayer(t *testing.T) {



	// setup configuration singleton
	cnf := dnsbroker.NewConfig("./dnstrak.toml")

	// setup hive singleton for publisher
	dnsbroker.NewHive("hive",cnf.Redis.Url)


	// create a publisher
	publisher := dnsbroker.NewPubSubBroker()

	// create the player
	player := dnsreplay.NewReplayer(pcapFile,publisher)

	player.Start()

}
