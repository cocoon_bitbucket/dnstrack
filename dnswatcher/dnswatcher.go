package main


import (

	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnswatch"

	"log"

)

/*
	dnswatcher ( a debug tool )
	  subscribe to pubsub channel "dnsguard.packets" and write it to influxdb


 */

func main() {


	log.Printf("starting dns watcher")
	//
	// setup the context
	//

	// create the configuration
	cnf :=dnsbroker.NewConfig("./dnstrak.toml")
	// create the hive
	dnsbroker.NewHive("hive",cnf.Redis.Url)


	// load context
	conf := dnsbroker.GetConfig()
	//hive := dnsbroker.GetHive()


	writer := dnswatch.NewInfluxDbWriter(conf.Influxdb.Url,"dns","dns")


	log.Printf("config version=%s" , conf.Version)
	//log.Printf("hive available for: %s", hive.Url)

	broker_available := true

	//err := hive.Check(false)
	//if err != nil {
	//	broker_available = false
	//	log.Printf("no broker available: standalone mode\n")
	//} else {
	//	broker_available = true
	//	log.Printf("broker [%s] available at %s\n" ,hive.Name, hive.Url)
	//}

	if broker_available == true {
		client := dnsbroker.NewRedisClient("me")
		client.Lock()
		client.Conn.Do("SET","dnswatch","starting")
		client.Subscribe("dnsguard.packets")
		client.Unlock()
		log.Printf("waiting for message on channel dnsguard.packets")
		for {
			rmessage := client.Receive()
			message:= rmessage.Data

			//spew.Dump(message)

			//log.Printf("received: \n%s\n ", message)
			if message != "" {

				typ := dnsbroker.DnsGuardQuestion{}
				data := typ.UnMarshal([]byte(message))
				//spew.Dump(data)
				log.Printf("%s,%s", data.QuestionName, data.EtldPlusOne)

				writer.AddDnsQuestion(*data)

			}
		}

	} else {

		log.Fatal("no broker available")
	}


}

