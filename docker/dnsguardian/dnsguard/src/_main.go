package main

import (
	"log"
	"os"
	"os/signal"

	"github.com/kung-foo/freki/netfilter"
	//"github.com/davecgh/go-spew/spew"
	"fmt"
	"github.com/google/gopacket"
	//"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/layers"
	//"net"
	//"time"
	//"strconv"
	//"golang.org/x/net/publicsuffix"
	//"strings"
	"github.com/davecgh/go-spew/spew"


	"golang.org/x/net/publicsuffix"
	"strings"

)

var (

	pp int =0

	SrcIP    	string
	DstIP    	string

	eth layers.Ethernet
	ip4 layers.IPv4
	ip6 layers.IPv6
	tcp layers.TCP
	udp layers.UDP
	dns layers.DNS

	payload gopacket.Payload




)

var BlackList = map[string]bool {

	"orangedns.club.": true,
	//"orangedns.info.": true,
}




func onErrorExit(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func onInterruptSignal(fn func()) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	go func() {
		<-sig
		fn()
	}()
}

// iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
// iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0


// a dummy ethernet header ( because netfilter squeeze it)
var ethHdr = []byte{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x08, 0x00,
}



func dissector(rawPacket *netfilter.RawPacket ) (map[string]interface{},error) {

	event := make(map[string]interface{})
	event["_verdict"] = "PASS"

	// add a dummy ethernet header because netfilter rawpacket does not have it
	// see: https://github.com/kung-foo/freki/blob/master/freki.go
	buffer := append(ethHdr, rawPacket.Data...)
	packet := gopacket.NewPacket(
		buffer,
		layers.LayerTypeIPv4,
		gopacket.DecodeOptions{Lazy: false, NoCopy: true},
	)

	var (
		eth  layers.Ethernet
		ip4   layers.IPv4
		ip6 layers.IPv6
		tcp  layers.TCP
		udp  layers.UDP
		dns  layers.DNS
		body gopacket.Payload
	)

	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&eth,
		&ip4,
		&ip6,
		&tcp,
		&udp,
		&dns,
		&body)

	var foundLayerTypes []gopacket.LayerType
	err := parser.DecodeLayers(packet.Data(), &foundLayerTypes)

	if err != nil {
		log.Printf("[freki   ] %v %v", err, foundLayerTypes)
		return event, err
	}

	isDns := false

	for _, layer := range foundLayerTypes {
		switch layer {

		case layers.LayerTypeIPv4:
			event["SrcIP"] = ip4.SrcIP.String()
			event["DstIP"] = ip4.DstIP.String()
		case layers.LayerTypeIPv6:
			event["SrcIP"] = ip6.SrcIP.String()
			event["DstIP"]= ip6.DstIP.String()
		case layers.LayerTypeUDP:
			event["dns.opt.udp_size"] = udp.Length
		case layers.LayerTypeDNS:

			isDns = true
			reply:= dns.QR



			if reply == false {

				// this is a simple query
				println("got a dns query\n")
				event["dns.id"] = dns.ID
				event["dns.op_code"] = dns.OpCode
				event["dns.response_code"] = dns.ResponseCode
				event["questions_count"] = dns.QDCount
				event["answers_count"] = dns.ANCount

				event["dns.Questions"]= dns.Questions
				event["dns.Answers"]= dns.Answers


				event["_verdict"] = check_blacklist_rules(dns)
				if event["_verdict"] == "DROP" {
					break
				}


				event["_verdict"] = check_local_rules(dns)
				println("verdict",event["_verdict"],"\n")

				if event["_verdict"] != "DROP" {

					for _, dnsQuestion := range dns.Questions {
						//name:= fmt.Sprintf("")
						name := string(dnsQuestion.Name)
						event["question1_name"] = name
						eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
						if err == nil {
							event["etld_plus_one"] = eTLDPlusOne + "."
						}
						//verdict := check_local_rules(dnsQuestion)
						//println("verdict",verdict,"\n")
						break
					}
				}


			} else {
				println("got a dns reply\n")
			}


		}
	}

	if isDns != true {
		//
	}


	return event, err
}


func check_local_rules(dns layers.DNS) string {

	verdict:= "PASS"
	// check dns event with local rules
	for _, dnsQuestion := range dns.Questions {
		//name:= fmt.Sprintf("")
		name := string(dnsQuestion.Name)
		typ := dnsQuestion.Type
		//other := dnsQuestion.Class
		println("dns question type: ",typ.String(),"\n")
		if typ == 16 {
			println("DNS QUESTION OF TYPE TXT ==> DROP :",name,"\n")
			verdict = "DROP"
			break
		}
	}
	return verdict
}

func check_blacklist_rules(dns layers.DNS) string {

	verdict:= "PASS"
	// check dns event with local rules
	for _, dnsQuestion := range dns.Questions {

		name := string(dnsQuestion.Name)
		eTLDPlusOne, err := publicsuffix.EffectiveTLDPlusOne(strings.TrimRight(name, "."))
		if err == nil {
			eTLDPlusOne = eTLDPlusOne + "."
		} else {
			eTLDPlusOne = "unknown" + "."
		}

		_, ok := BlackList[eTLDPlusOne]
		if ok {
			// domain is blacklistedv DROP it
			println("domain is blacklisted: ", eTLDPlusOne)
			verdict = "DROP"
			break

		}
	}
	return verdict
}






func handle_dns(q *netfilter.Queue) {


	go q.Run()

	//pp := 0

	pChan := q.Packets()

	for p := range pChan {
		//log.Printf("packet received here\n")

		//spew.Dump(p)
		// p is a netfilter rawPacket
		// p.ID is  netfilter packet id to set verdict on
		// p.Data is the raw content (ip layer) of the received packet )


		fmt.Printf("PKT [%03d] \n", p.ID)
		// fmt.Printf("------------------------------------------------------------------------\n id: %d\n", payload.Id)
		// fmt.Println(hex.Dump(payload.Data))
		// Decode a packet


		event,err := dissector(p)
		//println("dissector result:",err,"\n")
		if err != nil {
			println("dissector error result:",err,"\n")
		} else {
			// success we have a dns event
			spew.Dump(event)
		}


		if event["_verdict"] == "DROP" {
			println("DROP PACKET: ",p.ID)
			q.SetVerdict(p, netfilter.NF_DROP)
		} else {
			println("ACCEPT PACKET: ",p.ID)
			q.SetVerdict(p, netfilter.NF_ACCEPT)
		}
		pp++
		}



	//	q.SetVerdict(p, netfilter.NF_ACCEPT)
	//	pp++
	//}


}



func main() {
	q, err := netfilter.New(0, 100, netfilter.NF_DEFAULT_PACKET_SIZE)
	onErrorExit(err)

	//pp := 0

	onInterruptSignal(func() {
		q.Close()
		log.Printf("\n%d packets processed", pp)
		os.Exit(0)
	})


	handle_dns(q)

	//go q.Run()
	//
	//pChan := q.Packets()
	//
	//for p := range pChan {
	//	log.Printf("packet received\n")
	//	q.SetVerdict(p, netfilter.NF_ACCEPT)
	//	pp++
	//}
}
