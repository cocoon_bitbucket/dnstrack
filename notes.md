
dnstrack solution
=================

intercept traffic on udp 53 and decide to forward the dns packet or drop it
publish events of packet received to have some stats
analyse packet on the flight and perform local check
anlyse traffic over time to detect dns tunnels

components:

dnsguard
--------

* intercept traffic on udp port 53
* let the packet pass or drop it on behalf of local_check or broker_check
* dissect dns packet
* local_check: perform local checks
  * ok: transmit packet to the dns server
  * ko: drop the packet and log to dnsbroker
* broker_check: perform black list check ( with dns broker)

* trace to dnsbroker


dnswatch
--------

* subscribe to dns broker to get dns packet from dnsguard
* store packets to database (influxdb)
* periodically analyse traffic on database to setup blacklist domains (send ban to dnsbroker)


dnsbroker
---------
* communication nodes between dnsguard dnswatch and external world
* handle the broker (redis)
* subsribe to dnsguard to get dns packets from udp 53
  * store 


subcomponents:
* redis 
* influxdb




try live capture
================
needs root privillege to run


    cd /Users/cocoon/Go/src/bitbucket.org/cocoon_bitbucket/dnstrack

    export GOROOT=/usr/local/Cellar/go/1.6.2/libexec
    export GOPATH=/Users/cocoon/Go
    
    go run 
    go run liveCapture.go