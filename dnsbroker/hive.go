/*

	Hive a redis pool singleton

	// create the redis pool singleton
	hive:= NewHive("world" ," "redis://localhost:6379/1"")

	// get instance of redis pool
	hive= GetHive()

	// check connectivity
	hive.Check(true)

	// obtain a redis connection
	cnx := hive.NewConn()

	// obtain a redis pubsub connection
	ps := hive.NewPubsub()

	hive.Close()


 */
package dnsbroker


import (
	//"sync"
	//"../messenger"
	//"bitbucket.org/cocoon_bitbucket/go-actor/messenger"
	//"encoding/json"
	//"fmt"

	"github.com/garyburd/redigo/redis"
	//"time"
	"time"
	"log"

	"fmt"
	"sync"
)

var(
	instance *Hive
	//once sync.Once
)


type IHive interface {

	NewConn() redis.Conn
	NewPubsub() * redis.PubSubConn
	Check(raise bool) error
	Close()
}

type Hive struct {

	// name of the world eg ( Terre )
	Name string	 `json:"name"`
	// redis url of the world eg ( redis://192.168.1.100:6379/1 )
	Url  string      `json:"url"`
	// sync
	sync.Mutex
	// redis pool
	pool * redis.Pool

}


func newRedisPool(redisUrl string) *redis.Pool {
	// create a redis pool for a given redis url (eg redis://localhost:6379/0 )
	return &redis.Pool{
		MaxIdle: 3,
		IdleTimeout: 240 * time.Second,
		//Dial: func () (redis.Conn, error) { return redis.Dial("tcp", addr) },
		Dial: func () (redis.Conn, error) { return redis.DialURL(redisUrl) },
	}
}



// create a new singleton Hive
func NewHive(name string,url string) *Hive {
	// create hive singleton

	if instance != nil {
		// hive has already been assigned
		if name != instance.Name || url != instance.Url {
			log.Fatal(fmt.Sprintf("hive has already been assigned to %s,%s", instance.Name,instance.Url))
		} else {
			return instance
		}

	} else {
		// create a new hive singleton
		instance = NewFreeHive(name,url)
		//pool := newRedisPool(url)
		//instance = &Hive{
		//	name,
		//	url,
		//	sync.Mutex{},
		//	pool,
		//}
	}

	return instance
}


func NewFreeHive(name string,url string) * Hive {
	// create a non global hive
	pool := newRedisPool(url)
	hive := &Hive{
		name,
		url,
		sync.Mutex{},
		pool,
	}
	return hive

}


func ResetHive() {
	// allow reassignment for NewHive()
	instance= nil

}


func GetHive() *Hive {
	// get the hive singleton
	if instance  == nil {
		panic("no hive defined")
	}
	return instance
}


func (hive *Hive) Close() {

	// reset once
	//once = sync.Once{}
	instance = nil
	//atomic.StoreUint32(&once.done, 0)
}

func (hive *Hive) NewConn() redis.Conn{
	// get a redis connection from the pool
	return hive.pool.Get()
}

func (hive *Hive) NewPubsub() * redis.PubSubConn {
	cnx := hive.pool.Get()
	pubsub :=redis.PubSubConn{cnx}
	return &pubsub
}

func (hive *Hive) Check(raise bool) error {
	// check redis connectivity
	conn := hive.pool.Get()
	// check connectivity
	hive.Lock()
	_, err := conn.Do("PING")
	hive.Unlock()
	if err != nil {
		if raise == true {
			log.Fatal("Can't connect to the Redis database")
		}
	}
	conn.Close()
	return err

}



