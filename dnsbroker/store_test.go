package dnsbroker_test


import (

	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	"testing"
	"time"
	"github.com/stretchr/testify/assert"
)






func TestAddDomain(t *testing.T) {


	name := "hive"
	url := "redis://localhost:6379/0"


	dnsbroker.ResetHive()


	h := dnsbroker.NewHive(name,url)
	h2 := dnsbroker.GetHive()
	println(h==h2)



	store := dnsbroker.NewStore()

	ok := store.CheckDomain("orangedns.club.")
	println("ok: ", ok)
	assert.Equal(t,ok,true)

	store.SetDomain("orangedns.club.",5)

	ok = store.CheckDomain("orangedns.club.")
	println("ok: ", ok)
	assert.Equal(t,ok,false)

	time.Sleep(7 * time.Second)

	ok = store.CheckDomain("orangedns.club.")
	println("ok: ", ok)
	assert.Equal(t,ok,true)



	//_,err := store.GetDomain("orangedns.club.")
	//println(err)


	h2.Close()



}







