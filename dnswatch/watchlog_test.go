/*


redis_1       | 1:M 23 Jun 16:33:14.384 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
redis_1       | 1:M 23 Jun 16:33:14.384 # Server started, Redis version 3.2.9
redis_1       | 1:M 23 Jun 16:33:14.384 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
redis_1       | 1:M 23 Jun 16:33:14.384 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
redis_1       | 1:M 23 Jun 16:33:14.384 * The server is now ready to accept connections on port 6379
redis_1       | 1:M 23 Jun 16:38:15.089 * 10 changes in 300 seconds. Saving...
redis_1       | 1:M 23 Jun 16:38:15.091 * Background saving started by pid 10
redis_1       | 10:C 23 Jun 16:38:15.095 * DB saved on disk
redis_1       | 10:C 23 Jun 16:38:15.096 * RDB: 6 MB of memory used by copy-on-write


 */

package dnswatch_test



import (
	"testing"
	"bitbucket.org/cocoon_bitbucket/dnstrack/dnsbroker"
	//"bitbucket.org/cocoon_bitbucket/dnstrack/dnswatch"

	"log"
	//"github.com/davecgh/go-spew/spew"
	//"bitbucket.org/cocoon_bitbucket/dnstrack/dnswatch"
)

//var (
//	name = "hive"
//
//	redis_url = "redis://192.168.99.100:6379/0"
//
//	influxdb_url = "http://influxdb:8086"
//
//	broker_available = false
//)


func TestWatchLog(t *testing.T) {

	// create a reader for pubsub channel dnsguard.packets and log it
	//hive := dnsbroker.NewHive(name,redis_url)
	dnsbroker.NewHive(name,redis_url)

	broker_available = true

	//err := hive.Check(false)
	//if err != nil {
	//	broker_available = false
	//	log.Printf("no broker available: standalone mode\n")
	//} else {
	//	broker_available = true
	//	log.Printf("broker [%s] available at %s\n" ,hive.Name, hive.Url)
	//}
	//

	if broker_available == true {
		client := dnsbroker.NewRedisClient("me")
		client.Lock()
		client.Conn.Do("SET","dnswatch","starting")
		client.Subscribe("dnsguard.packets")
		client.Unlock()
		log.Printf("waiting for message on channel dnsguard.packets")
		for {
			rmessage := client.Receive()
			message:= rmessage.Data

			//spew.Dump(message)

			//log.Printf("received: \n%s\n ", message)
			if message != "" {

				typ := dnsbroker.DnsGuardQuestion{}
				data := typ.UnMarshal([]byte(message))
				//spew.Dump(data)
				log.Printf("%s,%s", data.QuestionName, data.EtldPlusOne)

			}
		}

	} else {

		log.Fatal("no broker available")
	}


}

